package com.bbva.techu.basic;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

public class CurrencyRound  {
	
	private CurrencyRound(){}

	public static BigDecimal round(Number number, Locale locale) {
		return BigDecimal.valueOf(number.doubleValue())
				.setScale(Currency.getInstance(locale).getDefaultFractionDigits(),BigDecimal.ROUND_UP);
	}
	
}
