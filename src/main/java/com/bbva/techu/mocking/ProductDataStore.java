package com.bbva.techu.mocking;

import java.util.Collection;

import com.bbva.techu.domain.Product;

public interface ProductDataStore {

	Collection<Product> getAll(String clientId);

	Product get(String id);

}
