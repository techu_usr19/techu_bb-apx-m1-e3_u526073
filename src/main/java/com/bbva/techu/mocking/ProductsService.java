package com.bbva.techu.mocking;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.bbva.techu.domain.Product;

public class ProductsService {
	
	@Inject
	private ProductDataStore products;

	public List<Product> getActiveProducts(String clientId) {
		List<Product> activeProducts = new ArrayList<>();
		for(Product product:products.getAll(clientId)){
			if(product.isActive()){
				activeProducts.add(product);
			}
		}
		return activeProducts;
	}

}
