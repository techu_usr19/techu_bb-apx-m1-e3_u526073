package com.bbva.techu.mocking;

public interface TransactionService {

	void transfer(String from, String to, double amount);

}
