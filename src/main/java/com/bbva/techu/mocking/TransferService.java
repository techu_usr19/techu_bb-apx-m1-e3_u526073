package com.bbva.techu.mocking;

import javax.inject.Inject;

import com.bbva.techu.domain.Product;

public class TransferService {
	
	@Inject
	private ProductDataStore productDataStore;
	
	@Inject
	private TransactionService transactionService;

	public void transfer(String fromId, String toId, double amount) {
		Product from = productDataStore.get(fromId);
		Product to = productDataStore.get(toId);
		
		transactionService.transfer(from.getIban(),to.getIban(),amount);
	}

}
