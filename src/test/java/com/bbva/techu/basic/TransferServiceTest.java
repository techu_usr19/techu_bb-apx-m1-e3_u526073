package com.bbva.techu.basic;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.bbva.techu.domain.Product;
import com.bbva.techu.mocking.ProductDataStore;
import com.bbva.techu.mocking.TransactionService;
import com.bbva.techu.mocking.TransferService;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

	@Mock
	private ProductDataStore productDataStore;
	
	@Mock
	private TransactionService transactionService;

	@InjectMocks
	public TransferService service;
	
	@Test
	public void transferServicetest() {
		// Preparar
		when (productDataStore.get("1")).thenReturn(product("1", true, "ES111000"));

		when (productDataStore.get("2")).thenReturn(product("2", true, "MX111000"));

		// Ejecutar
		service.transfer("1", "2", 50);
		
		// Comprobar
		Mockito.verify(transactionService, Mockito.times(1)).transfer("ES111000", "MX111000", 50);
	}

	private Product product (String it, boolean active, String iban) {
		Product prod = new Product();
		prod.setId(it);
		prod.setActive(active);
		prod.setIban(iban);
		return prod;
	}

}

