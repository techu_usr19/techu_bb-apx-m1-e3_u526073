package com.bbva.techu.basic;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyServiceTest {

	MyService componentUnderTest = null;
	
	@Before
	public void setup(){
	  	this.componentUnderTest=new MyService();
	}
	
	@Test
	public void testHello() {
		assertThat(componentUnderTest.sayHello(), equalTo("Hello"));
	}

	@Test
	public void testBye() {
		assertThat(componentUnderTest.sayBye(), equalTo("Bye"));
	}
}
