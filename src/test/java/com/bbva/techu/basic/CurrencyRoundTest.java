package com.bbva.techu.basic;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Locale;

import org.junit.Test;

public class CurrencyRoundTest {

	@Test
	public void testEnglishTwoDecimalsSame() {
		assertThat(CurrencyRound.round(5.23, Locale.US), equalTo(BigDecimal.valueOf(5.23)));
	}

	@Test
	public void testEnglishTwoDecimalsRound() {
		assertThat(CurrencyRound.round(5.232, Locale.US), equalTo(BigDecimal.valueOf(5.24)));
	}

	@Test
	public void testEnglishTwoDecimalsRoundMax() {
		assertThat(CurrencyRound.round(5.239, Locale.US), equalTo(BigDecimal.valueOf(5.24)));
	}

	@Test
	public void testJapanTwoDecimalsSame() {
		assertThat(CurrencyRound.round(5.23, Locale.JAPAN), equalTo(BigDecimal.valueOf(6)));
	}

}
