package com.bbva.techu.basic;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bbva.techu.domain.Product;
import com.bbva.techu.mocking.ProductDataStore;
import com.bbva.techu.mocking.ProductsService;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductDataStoreTest {

	@Mock
	public ProductDataStore products;
	
	@InjectMocks
	public ProductsService service;
	
	@Test
	public void testProductsService () {
		when(products.getAll("0001")).thenReturn(Arrays.asList(product("1",true),product("2",false)));
		
		List<Product> result = service.getActiveProducts("0001");
		
		assertThat(result, hasSize (1));
		assertThat(result.get(0).getId(), equalTo("1"));
	}

	private Product product (String it, boolean active) {
		Product prod = new Product();
		prod.setId(it);
		prod.setActive(active);
		return prod;
	}

	@Test
	public void testProductsServiceMore () {
		when(products.getAll("0001")).thenReturn(Arrays.asList(product("1",true),product("2",false),product("3",true)));
		
		List<Product> result = service.getActiveProducts("0001");
		
		assertThat(result, hasSize (2));
		assertThat(result.get(0).getId(), equalTo("1"));
		assertThat(result.get(1).getId(), equalTo("3"));
	}
}
